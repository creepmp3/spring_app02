package kr.co.hbilab.app;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.util.StopWatch;

/**
 * 2015. 5. 28.
 * @author user
 * 
 */
public class CheckTime {
    public Object logAround(ProceedingJoinPoint pjp) throws Throwable{
        // 메서드 이름 얻어오기
        String methodName = pjp.getSignature().getName();
        
        StopWatch sw = new StopWatch();
        sw.start();
        Object obj = pjp.proceed();
        sw.stop();
        System.out.println("처리시간 : " + sw.getTotalTimeSeconds());
        return obj;
    }
}
